#include <iostream>

void WriteNumber(bool is_evenNumb, short numb) {
	std::cout << "Numbers: ";
	for (short counter = (is_evenNumb ? 0 : 1); counter < numb; counter+=2) std::cout << counter << " ";
	std::cout << std::endl;
}

int main() {
	short number;
	std::cin >> number;
	std::cout << "Numbers: ";
	for (short counter = 0; counter < number; counter+=2) std::cout << counter << " ";;
	std::cout << std::endl;
	WriteNumber(true, number);
}